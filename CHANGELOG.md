[0.1.0]
* Initial release

[1.0.0]
* First stable release

[1.0.1]
* Add authentication support

[1.0.2]
* Fix documentation URL typo
 
